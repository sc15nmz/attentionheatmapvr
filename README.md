Clone the repo and run the project on Unreal Engine 4.19 as normal.

User Guide:
The controls that can be used while running the application are as follows:
- Q / Right Grip Button - Switch between heat map and normal material in real time
- P / Left Trigger - Pause the drawing of the heat map
- Left Grip Button - Reset VR position


This part will describe how to create an object capable of gathering the user's gaze, and get a coloured heat map, as well as a render target that can be exported for further analysis. Steps:
1. Inside the Heatmap/Renders folder, create a Render Target for the object. This is the black and white version of the heat map.
2. Inside the Heatmap/RenderTargetBake folder, create a second Render Target for the object. This will be the coloured version.
3. Inside the Heatmap/Blueprints folder, create a child blueprint of the BP_AttentionGatheringObject blueprint.
4. In the Viewport window, select PositionGatheringPlane on the left-hand side.
5. On the right-hand side, change the static mesh to the object you want.
6. In the Event Graph window, set up the logic for the render targets as seen in other children objects or Figure B.6 in the final report
7. Drag and drop the blueprint you created somewhere on the scene. It should now be capable of displaying a heat map for itself
8. Optionally, export the coloured heat map.